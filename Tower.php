<?php

/**
 * Created by PhpStorm.
 * User: allu
 * Date: 24/06/16
 * Time: 19:02
 */

require_once("Floor.php");

class Tower
{
  public $floors;

  /**
   * Tower constructor.
   */
  public function __construct($floorCount)
  {
  }

  public static function withFloorCount($floorCount)
  {
    $instance = new self();

    $floors = array();
    for ($i = $floorCount; $i > 0; $i--)
    {
      $max = 255;
      $min = 0;
      $color = array("r" => rand($min, $max), "g" => rand($min, $max), "b" => rand($min, $max),);
      $floors[] = new Floor($color, $i);
    }
    $instance->floors = $floors;
    return $instance;
  }


  public static function withString($string)
  {
    $instance = new self();
    $stringArray = explode(';', $string);
    $floors = array();
    foreach ($stringArray as $floorString)
    {
      $floorElems = explode(',', $floorString);
      $floors[] = new Floor(["r" => $floorElems[1], "g" => $floorElems[2], "b" => $floorElems[3]], $floorElems[0]);
    }
    $instance->floors = $floors;
    return $instance;
  }


  public function addFloor($addFloor)
  {
    if(!$this->floors || $addFloor->value > $this->floors[-1]->value)
    {
      $this->floors[] = $addFloor;
    }
    else
    {
      throw new RuntimeException("Bigger floor on top of smaller floor!");
    }
  }

  public function removeTopFloor()
  {
    if($this->floors)
    {
      return array_pop($this->floors);
    }
  }

  public function getTopFloorValue()
  {
    return $a = $this->floors ? $this->floors[count($this->floors) - 1]->getValue() : INF;

  }
  
  /**
   * Get i-th floor. If it does not exist, return empty string
   * @param $i
   * @return mixed
   */
  public function getFloor($i)
  {
    if($i <= $this->getHeight())
    {
      return $this->floors[$i];
    }
    else
    {
      return NULL;
    }
  }


  public function getHeight()
  {
    return count($this->floors);
  }


  function __toString()
  {
    $str = "";

    foreach ($this->floors as $floor)
    {
      $str .= $floor->__toString() . ";";
    }
    return rtrim($str, ";");
  }
}



