<?php

/**
 * Created by PhpStorm.
 * User: allu
 * Date: 24/06/16
 * Time: 18:52
 */
class Floor
{
  public $value;
  public $color;

  /**
   * Floor constructor.
   * @param $color
   * @param $value
   */

  public function __construct($color, $value)
  {
    $this->color = $color;
    $this->value = $value;
  }

//  public function __construct()
//  {
//
//  }

  /**
   * @return mixed
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * @param mixed $value
   */
  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getColor()
  {
    return $this->color;
  }

  /**
   * @param mixed $color
   */
  public function setColor($color)
  {
    $this->color = $color;
    return $this;
  }

  

public function  getFloorWidth()
{
  return $this->getValue()*50+50;
}

public function colorText()
  {
    $string='<td align="center">  
    <svg width="%s" height="30">
  <rect width="%s" height="30" style="fill:rgb(%s,%s,%s);stroke-width:3;stroke:rgb(0,0,0)" />
</svg> 
    </td>';
    return vsprintf($string, array(
                                    $this->getFloorWidth() +10,
                                    $this->getFloorWidth(),
                                    $this->getColor()["r"],
                                    $this->getColor()["g"],
                                    $this->getColor()["b"],
    ));

  }

  function __toString()
  {
    return $this->value . "," .  implode(",",$this->color) ;
  }


}