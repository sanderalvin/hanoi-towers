<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require 'Floor.php';
require 'Tower.php';

if (!empty($_POST)) {
	$left = $_COOKIE["tower1"] ? Tower::withString($_COOKIE["tower1"]) : Tower::withFloorCount(0);
	$middle = $_COOKIE["tower2"] ? Tower::withString($_COOKIE["tower2"]) : Tower::withFloorCount(0);
	$right = $_COOKIE["tower3"] ? Tower::withString($_COOKIE["tower3"]) : Tower::withFloorCount(0);

	if ($_POST["left_to_middle"]) {
		$middle->addFloor($left->removeTopFloor());
	} else if ($_POST["left_to_right"]) {
		$right->addFloor($left->removeTopFloor());
	} else if ($_POST["middle_to_left"]) {
		$left->addFloor($middle->removeTopFloor());
	} elseif ($_POST["middle_to_right"]) {
		$right->addFloor($middle->removeTopFloor());
	} elseif ($_POST["right_to_middle"]) {
		$middle->addFloor($right->removeTopFloor());
	} elseif ($_POST["right_to_left"]) {
		$left->addFloor($right->removeTopFloor());
	}

} else {
	$left = Tower::withFloorCount($_GET["floors"]);
	$middle = Tower::withFloorCount(0);
	$right = Tower::withFloorCount(0);
}

setcookie("tower1", $left->__toString());
setcookie("tower2", $middle->__toString());
setcookie("tower3", $right->__toString());

$win = $left->getHeight() + $middle->getHeight() === 0 ? true : false;

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <script src="js/bootstrap.min.js"></script>
</head>

<body>
<hr width="5000px">
<?php

if ($win) {
	echo "<p> You have won the game. However, have the lambs stopped screaming? <br>
            Yours dearly,<br>
            Mr. Snake.
            </p>";
}

?>

<div class="container">

  <table>
    <col align="center">
    <col align="center">
    <col align="center">


    <tr>
      <?php

function buttonClickRules($map_array) {
	$arr = [];
	foreach ($map_array as $index => $item) {
		foreach ($map_array as $index2 => $item2) {
			if ($index == $index2) {
				continue;
			}
			$a = $item->getTopFloorValue();
			$b = $item2->getTopFloorValue();
			$arr[] = $a < $b ? true : false;
		}
	}
	return $arr;
}

$map_array = ["left" => $left, "middle" => $middle, "right" => $right];
$rules = buttonClickRules($map_array);
$loopLength = max(array($left->getHeight(), $middle->getHeight(), $right->getHeight())) - 1;
for ($i = $loopLength; $i >= 0; $i--) {
	// $map_array=["left"=>$left, "middle"=> $middle, "right"=>$right]; //TODO remove

	$f1 = $left->getFloor($i);
	$f2 = $middle->getFloor($i);
	$f3 = $right->getFloor($i);

	echo '<tr>';
	echo ($f1 ? $f1->colorText() : "<td></td>" . PHP_EOL);
	echo ($f2 ? $f2->colorText() : "<td></td>" . PHP_EOL);
	echo ($f3 ? $f3->colorText() : "<td></td>" . PHP_EOL);
	echo '</tr>';
}

$maxWidth = max([$f1 ? $f1->getFloorWidth() : 0,
	$f2 ? $f2->getFloorWidth() : 0,
	$f3 ? $f3->getFloorWidth() : 0,
]) + 20;

echo '<tr>';
echo '<td><hr width="' . $maxWidth . 'px"> </td>' . PHP_EOL;
echo '<td><hr width="' . $maxWidth . 'px"> </td>' . PHP_EOL;
echo '<td><hr width="' . $maxWidth . 'px"> </td>' . PHP_EOL;
echo '</tr>';

?>

    <tr>
      <form method="post">

        <td align="center">
          <input type="submit" <?php echo !array_shift($rules) ? "disabled" : "" ?> name="left_to_middle" value=">">
          <br>
          <input type="submit" <?php echo !array_shift($rules) ? "disabled" : "" ?> name="left_to_right" value=">>">


        </td>
        <td align="center">
          <input type="submit" <?php echo !array_shift($rules) ? "disabled" : "" ?> name="middle_to_left" value="<">
          <input type="submit" <?php echo !array_shift($rules) ? "disabled" : "" ?> name="middle_to_right" value=">">
        </td>
        <td align="center">
          <input type="submit" <?php echo !array_pop($rules) ? "disabled" : "" ?> name="right_to_middle" value="<">
          <br>
          <input type="submit" <?php echo !array_shift($rules) ? "disabled" : "" ?> name="right_to_left" value="<<">


        </td>


      </form>
    <tr>


    </tr>

  </table>

</div> <!-- /container -->
</body>
</html>
